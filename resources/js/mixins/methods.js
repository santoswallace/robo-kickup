export default {

    data() {
        return {
            audio_correspondido: null,
            events: [],
            account: [],
            competions: [],
            listClearedOrders: [],
            bots: [],
            run_bots: false,
        }
    },

    methods: {
        getEvents() {

            axios.get('./get/events').then(res => {
                this.events = res.data
            }).catch(e => {

                console.log(e);
            });
        },

        async getCompetions() {

            axios.get('./get/competions').then(res => {
                this.competions = res.data

            }).catch(e => {
                console.log(e);
            });
        },

        getAccount() {

            axios.get('./get/account').then(res => {
                this.account = res.data

            }).catch(e => {

                console.log(e);
            });
        },

        getBots() {

            axios.get('./bot/get', this.formBot).then(res => {
                this.bots = res.data;
            }).catch(e => {
                console.log(e);
            });
        },

        async bot_run_in() {

            this.run_bots = true;
            await axios.get('./bot/run/in').then(res => {
                if (res.data) {
                    document.getElementById("som_entrada").play();
                }
            }).then( () => {
                this.run_bots = false;
            }).catch(e => {
                console.log(e);
            });
        },

        async bot_run_exit() {
            this.run_bots = true;
            await axios.get('./bot/run/exit').then(res => {
                if (res.data) {
                    document.getElementById("som_saida").play();
                }
            }).then(() => {
                this.run_bots = false;
            }).catch(e => {
                console.log(e);
            });
        },

        getlistClearedOrders() {

            axios.get('./bet/listClearedOrders').then(res => {
                this.listClearedOrders = res.data

            }).catch(e => {
                console.log(e);
            });
        },

        message(title, message, type, duration = null){
            this.$notify({
                group: 'submit',
                clean: true
            })
            this.$notify({
                group: 'submit',
                title: title,
                text: message,
                type: type,
                duration: duration ? duration : 3000
            });
        },

        getFullEvents(){
            axios.get('./probabilidades/get/events/full').then(res => {
                this.eventFull = res.data;
                this.loadingEvent = false;
            });
        }
    }
}