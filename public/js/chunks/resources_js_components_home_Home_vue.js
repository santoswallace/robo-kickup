(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_home_Home_vue"],{

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mixins_methods__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/methods */ "./resources/js/mixins/methods.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mixins: [_mixins_methods__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      loading: false,
      SelectBot: [],
      allCompetions: null,
      formBot: {
        status: null,
        name: null,
        type: null,
        value: null,
        market: null,
        selection: null,
        odd_min: null,
        odd_max: null,
        time_min: null,
        time_max: null,
        liquidity: null,
        max_gap: null,
        score_home: null,
        score_away: null,
        active_exit: null,
        gap_exit: null,
        max_value_day: null,
        competions: [],
        statistics: [],
        time_exit: null,
        exit_take_gol: null,
        porcent_profit: null,
        porcent_loss: null,
        probability: null
      }
    };
  },
  watch: {
    'formBot.active_exit': function formBotActive_exit(value) {
      if (value == 0) {
        this.formBot.gap_exit = null;
      }
    },
    'allCompetions': function allCompetions(value) {
      this.formBot.competions = [];
    }
  },
  mounted: function mounted() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _this.getCompetions();

            case 2:
              _this.getEvents();

              setInterval(function () {
                _this.getEvents();
              }, 120000);
              setInterval( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
                return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (_this.run_bots) {
                          _context.next = 5;
                          break;
                        }

                        _context.next = 3;
                        return _this.bot_run_in();

                      case 3:
                        _context.next = 5;
                        return _this.bot_run_exit();

                      case 5:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              })), 60000);

              _this.getAccount();

              setInterval(function () {
                if (!_this.run_bots) {
                  _this.getAccount();
                }
              }, 60000);

              _this.getBots();

              _this.getlistClearedOrders();

              $('#newbot').modal({
                show: false,
                backdrop: 'static'
              });

            case 10:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }))();
  },
  methods: {
    resetModalBot: function resetModalBot() {
      this.formBot = {
        status: null,
        name: null,
        type: null,
        value: null,
        market: null,
        selection: null,
        odd_min: null,
        odd_max: null,
        time_min: null,
        time_max: null,
        liquidity: null,
        max_gap: null,
        score_home: null,
        score_away: null,
        active_exit: null,
        gap_exit: null,
        max_value_day: null,
        competions: [],
        statistics: [],
        time_exit: null,
        exit_take_gol: null,
        porcent_profit: null,
        porcent_loss: null,
        probability: null
      }, this.allCompetions = null;
    },
    submitBot: function submitBot() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        var data;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this2.loading = true;
                data = _this2.formBot;
                data.allCompetions = _this2.allCompetions;
                _context3.next = 5;
                return axios.post("./bot/".concat(_this2.formBot.id ? "update" : "store"), data).then(function (res) {
                  _this2.loading = false;

                  if (_this2.formBot.id) {
                    _this2.bots[_this2.SelectBot.index] = res.data;
                  } else {
                    _this2.bots.push(res.data);
                  }
                }).then(function () {
                  _this2.resetModalBot();

                  $('#newbot').modal('hide');
                })["catch"](function (e) {
                  _this2.loading = false;
                  console.log(e);
                });

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    editBot: function editBot(bot) {
      this.resetModalBot();
      this.formBot = bot;
    },
    addStatistic: function addStatistic() {
      this.formBot.statistics.push({
        statistic: null,
        side: null,
        min: null,
        max: null
      });
    },
    removeStatistic: function removeStatistic() {
      var index = this.formBot.statistics.length - 1;

      if (!this.formBot.statistics[index].id) {
        this.formBot.statistics.pop();
      }
    }
  }
});

/***/ }),

/***/ "./resources/js/mixins/methods.js":
/*!****************************************!*\
  !*** ./resources/js/mixins/methods.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      audio_correspondido: null,
      events: [],
      account: [],
      competions: [],
      listClearedOrders: [],
      bots: [],
      run_bots: false
    };
  },
  methods: {
    getEvents: function getEvents() {
      var _this = this;

      axios.get('./get/events').then(function (res) {
        _this.events = res.data;
      })["catch"](function (e) {
        console.log(e);
      });
    },
    getCompetions: function getCompetions() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                axios.get('./get/competions').then(function (res) {
                  _this2.competions = res.data;
                })["catch"](function (e) {
                  console.log(e);
                });

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    getAccount: function getAccount() {
      var _this3 = this;

      axios.get('./get/account').then(function (res) {
        _this3.account = res.data;
      })["catch"](function (e) {
        console.log(e);
      });
    },
    getBots: function getBots() {
      var _this4 = this;

      axios.get('./bot/get', this.formBot).then(function (res) {
        _this4.bots = res.data;
      })["catch"](function (e) {
        console.log(e);
      });
    },
    bot_run_in: function bot_run_in() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this5.run_bots = true;
                _context2.next = 3;
                return axios.get('./bot/run/in').then(function (res) {
                  if (res.data) {
                    document.getElementById("som_entrada").play();
                  }
                }).then(function () {
                  _this5.run_bots = false;
                })["catch"](function (e) {
                  console.log(e);
                });

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    bot_run_exit: function bot_run_exit() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this6.run_bots = true;
                _context3.next = 3;
                return axios.get('./bot/run/exit').then(function (res) {
                  if (res.data) {
                    document.getElementById("som_saida").play();
                  }
                }).then(function () {
                  _this6.run_bots = false;
                })["catch"](function (e) {
                  console.log(e);
                });

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    getlistClearedOrders: function getlistClearedOrders() {
      var _this7 = this;

      axios.get('./bet/listClearedOrders').then(function (res) {
        _this7.listClearedOrders = res.data;
      })["catch"](function (e) {
        console.log(e);
      });
    },
    message: function message(title, _message, type) {
      var duration = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      this.$notify({
        group: 'submit',
        clean: true
      });
      this.$notify({
        group: 'submit',
        title: title,
        text: _message,
        type: type,
        duration: duration ? duration : 3000
      });
    },
    getFullEvents: function getFullEvents() {
      var _this8 = this;

      axios.get('./probabilidades/get/events/full').then(function (res) {
        _this8.eventFull = res.data;
        _this8.loadingEvent = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.card-header[data-v-1f26c2f4] {\n    padding:5px !important;\n    background:rgb(12, 4, 114) !important;\n    color:#FFF !important;\n    font-size:13px !important;\n    font-weight:bold !important;\n    text-transform:uppercase !important;\n    align-items: center;\n}\nbody[data-v-1f26c2f4]{   \n    font-size: 14px !important;\n    font-family: 'Ubuntu', sans-serif !important;\n}\nlabel[data-v-1f26c2f4] {\n    color:rgb(0, 0, 0);\n    font-weight: normal;\n    text-transform:uppercase !important;\n    font-size:11px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/***/ ((module) => {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : 0
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_1f26c2f4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_1f26c2f4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_1f26c2f4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/components/home/Home.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/home/Home.vue ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=1f26c2f4&scoped=true& */ "./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/components/home/Home.vue?vue&type=script&lang=js&");
/* harmony import */ var _Home_vue_vue_type_style_index_0_id_1f26c2f4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& */ "./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "1f26c2f4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/home/Home.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/home/Home.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/home/Home.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_1f26c2f4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=style&index=0&id=1f26c2f4&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_1f26c2f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Home.vue?vue&type=template&id=1f26c2f4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Home.vue?vue&type=template&id=1f26c2f4&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container-fluid" },
    [
      _c("div", { staticClass: "row justify-content-center" }, [
        _vm._m(0),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "card" }, [
            _c(
              "div",
              { staticClass: "card-header d-flex justify-content-between" },
              [
                _c("b", [_vm._v("Resumo Financeiro")]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-sm btn-info",
                    on: {
                      click: function($event) {
                        return _vm.getAccount()
                      }
                    }
                  },
                  [_vm._v("Atualizar")]
                )
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c("b", [_vm._v("Responsável: ")]),
              _vm._v(
                " " +
                  _vm._s(_vm.account.firstName ? _vm.account.firstName : null) +
                  " " +
                  _vm._s(_vm.account.lastName ? _vm.account.lastName : null) +
                  " "
              ),
              _c("br"),
              _vm._v(" "),
              _c("b", [_vm._v("Saldo: ")]),
              _vm._v(
                " " +
                  _vm._s(
                    _vm.account.availableToBetBalance
                      ? _vm.account.availableToBetBalance.toLocaleString(
                          "pt-br",
                          { style: "currency", currency: "BRL" }
                        )
                      : "R$" + 0
                  ) +
                  " "
              ),
              _c("br"),
              _vm._v(" "),
              _c("b", [_vm._v("Saldo em mercado: ")]),
              _vm._v(
                " " +
                  _vm._s(
                    _vm.account.exposure
                      ? _vm.account.exposure.toLocaleString("pt-br", {
                          style: "currency",
                          currency: "BRL"
                        })
                      : "R$ " + 0
                  ) +
                  "\n                "
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12 mt-3" }, [
          _c("div", { staticClass: "card" }, [
            _c(
              "div",
              { staticClass: "card-header d-flex justify-content-between" },
              [
                _vm._v("Bots "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-sm btn-success",
                    attrs: { "data-toggle": "modal", "data-target": "#newbot" },
                    on: {
                      click: function($event) {
                        return _vm.resetModalBot()
                      }
                    }
                  },
                  [_vm._v("Novo Bot")]
                )
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  { staticClass: "table table-striped text-center " },
                  [
                    _vm._m(2),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.bots, function(bot, indexBot) {
                        return _c("tr", { key: bot.id }, [
                          _c("td", [
                            _c(
                              "span",
                              {
                                class:
                                  "badge badge-" +
                                  (bot.status == 1 ? "success" : "danger")
                              },
                              [
                                _vm._v(
                                  "\n                                            " +
                                    _vm._s(
                                      bot.status === 1 ? "Ativo" : "Inativo"
                                    ) +
                                    "\n                                        "
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [_c("b", [_vm._v(_vm._s(bot.name))])]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                bot.value
                                  ? bot.value.toLocaleString("pt-br", {
                                      style: "currency",
                                      currency: "BRL"
                                    })
                                  : "R$" + 0
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.odd_min))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.odd_max))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.score_home))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.score_away))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.time_min))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(bot.time_max))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                bot.liquidity
                                  ? bot.liquidity.toLocaleString("pt-br", {
                                      style: "currency",
                                      currency: "BRL"
                                    })
                                  : null
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-sm btn-secondary",
                                attrs: {
                                  "data-toggle": "modal",
                                  "data-target": "#newbot"
                                },
                                on: {
                                  click: function($event) {
                                    _vm.SelectBot = { index: indexBot }
                                    _vm.editBot(bot)
                                  }
                                }
                              },
                              [_vm._v("Editar")]
                            )
                          ])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12 col-md-12 mt-3" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-header " }, [
              _vm._v(_vm._s(_vm.events.length) + " - Monitorando "),
              _c("b", { staticClass: "text-success" }, [_vm._v("Ao vivo")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _vm.events.length <= 0
                ? _c("p", { staticClass: "text-center" }, [
                    _c("b", [
                      _vm._v(
                        "Não existe jogos dentro da suas configurações de bot(s)"
                      )
                    ])
                  ])
                : _c("table", { staticClass: "table table-striped" }, [
                    _vm._m(3),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.events, function(event) {
                        return _c(
                          "tr",
                          {
                            key: event.event.id,
                            staticClass: "bg-success text-white"
                          },
                          [
                            _c("th", { attrs: { scope: "row" } }, [
                              _vm._v(_vm._s(event.event.id))
                            ]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(event.event.name))]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(
                                _vm._s(
                                  event.event.statistics
                                    ? event.event.statistics.score.home.score
                                    : "-"
                                ) +
                                  " x " +
                                  _vm._s(
                                    event.event.statistics
                                      ? event.event.statistics.score.away.score
                                      : "-"
                                  ) +
                                  " "
                              )
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(
                                _vm._s(
                                  event.event.statistics
                                    ? event.event.statistics.elapsedRegularTime
                                    : "-"
                                ) + "'"
                              )
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-12 col-md-12 mt-3" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-header" }, [
              _vm._v("Últimas entradas (1) Dias")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table table-striped" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.listClearedOrders, function(
                      listCleared,
                      indexOrders
                    ) {
                      return _c(
                        "tr",
                        {
                          key: indexOrders,
                          staticClass: "text-white text-center",
                          style:
                            "" +
                            (listCleared.side == "LAY"
                              ? "background: #F694A9 !important;"
                              : "background:#116196 !important;")
                        },
                        [
                          _c("th", { attrs: { scope: "row" } }, [
                            _vm._v(_vm._s(listCleared.eventId))
                          ]),
                          _vm._v(" "),
                          _c("th", { attrs: { scope: "row" } }, [
                            _vm._v(_vm._s(listCleared.marketId))
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(listCleared.itemDescription.eventDesc)
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(listCleared.itemDescription.marketDesc)
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(listCleared.side))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                listCleared.priceMatched
                                  ? listCleared.priceMatched.toLocaleString(
                                      "pt-br",
                                      { style: "currency", currency: "BRL" }
                                    )
                                  : "R$ " + 0
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                listCleared.profit
                                  ? listCleared.profit.toLocaleString("pt-br", {
                                      style: "currency",
                                      currency: "BRL"
                                    })
                                  : "R$ " + 0
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(listCleared.placedDate))])
                        ]
                      )
                    }),
                    0
                  )
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "newbot",
            tabindex: "-1",
            "aria-labelledby": "newbotLabel",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-xl",
              staticStyle: { width: "100% !important" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(5),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c(
                    "div",
                    { staticClass: "row" },
                    [
                      _vm._m(6),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(7),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formBot.status,
                                  expression: "formBot.status"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.formBot,
                                    "status",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "1" } }, [
                                _vm._v("Ativo")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "0" } }, [
                                _vm._v("Inativo")
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(8),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.name,
                                expression: "formBot.name"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "name",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(9),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.value,
                                expression: "formBot.value"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.value },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "value",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(10),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formBot.market,
                                  expression: "formBot.market"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.formBot,
                                    "market",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "MATCH_ODDS" } }, [
                                _vm._v("Resultado Final")
                              ]),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "CORRECT_SCORE" } },
                                [_vm._v("Resultado Correto")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_05" } },
                                [_vm._v("Mais/Menos Gols 0.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_15" } },
                                [_vm._v("Mais/Menos Gols 1.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_25" } },
                                [_vm._v("Mais/Menos Gols 2.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_35" } },
                                [_vm._v("Mais/Menos Gols 3.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_45" } },
                                [_vm._v("Mais/Menos Gols 4.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_55" } },
                                [_vm._v("Mais/Menos Gols 5.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_65" } },
                                [_vm._v("Mais/Menos Gols 6.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_75" } },
                                [_vm._v("Mais/Menos Gols 7.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "OVER_UNDER_85" } },
                                [_vm._v("Mais/Menos Gols 8.5")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_05" } },
                                [_vm._v("Mais/Menos Gols 0.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_15" } },
                                [_vm._v("Mais/Menos Gols 1.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_25" } },
                                [_vm._v("Mais/Menos Gols 2.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_35" } },
                                [_vm._v("Mais/Menos Gols 3.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_45" } },
                                [_vm._v("Mais/Menos Gols 4.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_55" } },
                                [_vm._v("Mais/Menos Gols 5.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_65" } },
                                [_vm._v("Mais/Menos Gols 6.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_75" } },
                                [_vm._v("Mais/Menos Gols 7.5 HT")]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                { attrs: { value: "FIRST_HALF_GOALS_85" } },
                                [_vm._v("Mais/Menos Gols 8.5 HT")]
                              )
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(11),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formBot.type,
                                  expression: "formBot.type"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.formBot,
                                    "type",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "back" } }, [
                                _vm._v("Back")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "lay" } }, [
                                _vm._v("Lay")
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(12),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formBot.selection,
                                  expression: "formBot.selection"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.formBot,
                                    "selection",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _vm.formBot.market == "MATCH_ODDS"
                                ? _c("option", { attrs: { value: "0" } }, [
                                    _vm._v("Casa")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "MATCH_ODDS"
                                ? _c("option", { attrs: { value: "1" } }, [
                                    _vm._v("Visitante")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "MATCH_ODDS"
                                ? _c("option", { attrs: { value: "2" } }, [
                                    _vm._v("Empate")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market != "MATCH_ODDS" &&
                              _vm.formBot.market != "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "0" } }, [
                                    _vm._v("Under")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market != "MATCH_ODDS" &&
                              _vm.formBot.market != "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "1" } }, [
                                    _vm._v("Over")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "0" } }, [
                                    _vm._v("0 - 0")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "1" } }, [
                                    _vm._v("0 - 1")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "2" } }, [
                                    _vm._v("0 - 2")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "3" } }, [
                                    _vm._v("0 - 3")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "4" } }, [
                                    _vm._v("1 - 0")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "5" } }, [
                                    _vm._v("1 - 1")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "6" } }, [
                                    _vm._v("1 - 2")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "7" } }, [
                                    _vm._v("1 - 3")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "8" } }, [
                                    _vm._v("2 - 0")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "9" } }, [
                                    _vm._v("2 - 1")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "10" } }, [
                                    _vm._v("2 - 2")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.formBot.market == "CORRECT_SCORE"
                                ? _c("option", { attrs: { value: "11" } }, [
                                    _vm._v("2 - 3")
                                  ])
                                : _vm._e()
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._m(13),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(14),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.probability,
                                expression: "formBot.probability"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.probability },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "probability",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(15),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.odd_min,
                                expression: "formBot.odd_min"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.odd_min },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "odd_min",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(16),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.odd_max,
                                expression: "formBot.odd_max"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.odd_max },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "odd_max",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(17),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.time_min,
                                expression: "formBot.time_min"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.time_min },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "time_min",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(18),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.time_max,
                                expression: "formBot.time_max"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.time_max },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "time_max",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(19),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.liquidity,
                                expression: "formBot.liquidity"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.liquidity },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "liquidity",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(20),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.max_gap,
                                expression: "formBot.max_gap"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.max_gap },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "max_gap",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(21),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.score_home,
                                expression: "formBot.score_home"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.score_home },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "score_home",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(22),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.score_away,
                                expression: "formBot.score_away"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.score_away },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "score_away",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._m(23),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-6" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(24),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.allCompetions,
                                  expression: "allCompetions"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.allCompetions = $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "1" } }, [
                                _vm._v("Sim")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "0" } }, [
                                _vm._v("Não")
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      !_vm.allCompetions || _vm.allCompetions != 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _vm._m(25),
                                _vm._v(" "),
                                _c("v-select", {
                                  attrs: {
                                    disabled: _vm.loading,
                                    multiple: "",
                                    options: _vm.competions,
                                    label: "name"
                                  },
                                  model: {
                                    value: _vm.formBot.competions,
                                    callback: function($$v) {
                                      _vm.$set(_vm.formBot, "competions", $$v)
                                    },
                                    expression: "formBot.competions"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 py-1" }, [
                        _c(
                          "span",
                          {
                            staticClass:
                              "p-2 bg-warning w-100 d-block my-3 text-white d-flex justify-content-between align-items-center"
                          },
                          [
                            _c("b", [_vm._v("Estatísticas")]),
                            _vm._v(" "),
                            _c("div", [
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-sm btn-primary",
                                  on: {
                                    click: function($event) {
                                      return _vm.addStatistic()
                                    }
                                  }
                                },
                                [_c("b", [_vm._v("+")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-sm btn-secondary",
                                  on: {
                                    click: function($event) {
                                      return _vm.removeStatistic()
                                    }
                                  }
                                },
                                [_c("b", [_vm._v("-")])]
                              )
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.formBot.statistics, function(
                        statistic,
                        indice
                      ) {
                        return _c(
                          "div",
                          { key: indice, staticClass: "col-12 row " },
                          [
                            _c("div", { staticClass: "col-12 col-md-3" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _vm._m(26, true),
                                _vm._v(" "),
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value:
                                          _vm.formBot.statistics[indice]
                                            .statistic,
                                        expression:
                                          "formBot.statistics[indice].statistic"
                                      }
                                    ],
                                    staticClass: "form-control form-control-sm",
                                    on: {
                                      change: function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.formBot.statistics[indice],
                                          "statistic",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      }
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("Selecione")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Penalties" } },
                                      [_vm._v("Penaltis")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "YellowCards" } },
                                      [_vm._v("Cartões Amarelos")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "RedCards" } },
                                      [_vm._v("Cartões Vermelhos")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Substitutions" } },
                                      [_vm._v("Substituições")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "ShotsOnTarget" } },
                                      [_vm._v("Chutes no alvo")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "ShotsOffTarget" } },
                                      [_vm._v("Chutes Fora do Alvo")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "BlockedShots" } },
                                      [_vm._v("Chutes Bloqueados")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "TotalShots" } },
                                      [_vm._v("Total de chutes")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Corners" } },
                                      [_vm._v("Escanteio")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Fouls" } },
                                      [_vm._v("Faltas")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Offsides" } },
                                      [_vm._v("Laterais")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "GoalKicks" } },
                                      [_vm._v("Chutes a gol")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "FreeKicks" } },
                                      [_vm._v("Tiro Livre ")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      {
                                        attrs: { value: "DangerousFreeKicks" }
                                      },
                                      [_vm._v("Chutes Perigosos ")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Attacks" } },
                                      [_vm._v("Ataques ")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "DangerousAttacks" } },
                                      [_vm._v("Ataques Perigosos ")]
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-12 col-md-3" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _vm._m(27, true),
                                _vm._v(" "),
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value:
                                          _vm.formBot.statistics[indice].side,
                                        expression:
                                          "formBot.statistics[indice].side"
                                      }
                                    ],
                                    staticClass: "form-control form-control-sm",
                                    on: {
                                      change: function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.formBot.statistics[indice],
                                          "side",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      }
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("Selecione")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "Home" } }, [
                                      _vm._v("Casa")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "Away" } }, [
                                      _vm._v("Fora")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "Home+Away" } },
                                      [_vm._v("Casa + Fora")]
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-12 col-md-3" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _vm._m(28, true),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.formBot.statistics[indice].min,
                                      expression:
                                        "formBot.statistics[indice].min"
                                    }
                                  ],
                                  staticClass: "form-control form-control-sm",
                                  attrs: { type: "text" },
                                  domProps: {
                                    value: _vm.formBot.statistics[indice].min
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.formBot.statistics[indice],
                                        "min",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-12 col-md-3" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _vm._m(29, true),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.formBot.statistics[indice].max,
                                      expression:
                                        "formBot.statistics[indice].max"
                                    }
                                  ],
                                  staticClass: "form-control form-control-sm",
                                  attrs: { type: "text" },
                                  domProps: {
                                    value: _vm.formBot.statistics[indice].max
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.formBot.statistics[indice],
                                        "max",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          ]
                        )
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12" }, [
                        _c(
                          "span",
                          {
                            staticClass:
                              "p-2 bg-danger w-100 d-block my-3 text-white"
                          },
                          [
                            _c("b", [
                              _vm._v(
                                "Critérios de Saída: " +
                                  _vm._s(
                                    _vm.formBot.active_exit &&
                                      _vm.formBot.active_exit == 1
                                      ? "ATIVO"
                                      : "INATIVO"
                                  ) +
                                  " "
                              )
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(30),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formBot.active_exit,
                                  expression: "formBot.active_exit"
                                }
                              ],
                              staticClass: "form-control form-control-sm",
                              attrs: { disabled: _vm.loading },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.formBot,
                                    "active_exit",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "1" } }, [
                                _vm._v("Ativo")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "0" } }, [
                                _vm._v("Inativo")
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.formBot.active_exit && _vm.formBot.active_exit == 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _vm._m(31),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.formBot.gap_exit,
                                    expression: "formBot.gap_exit"
                                  }
                                ],
                                staticClass: "form-control form-control-sm",
                                attrs: { disabled: _vm.loading, type: "text" },
                                domProps: { value: _vm.formBot.gap_exit },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.formBot,
                                      "gap_exit",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.formBot.active_exit && _vm.formBot.active_exit == 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _vm._m(32),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.formBot.time_exit,
                                    expression: "formBot.time_exit"
                                  }
                                ],
                                staticClass: "form-control form-control-sm",
                                attrs: { disabled: _vm.loading, type: "text" },
                                domProps: { value: _vm.formBot.time_exit },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.formBot,
                                      "time_exit",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.formBot.active_exit && _vm.formBot.active_exit == 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _vm._m(33),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.formBot.exit_take_gol,
                                      expression: "formBot.exit_take_gol"
                                    }
                                  ],
                                  staticClass: "form-control form-control-sm",
                                  attrs: { disabled: _vm.loading },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        _vm.formBot,
                                        "exit_take_gol",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c("option", { attrs: { value: "1" } }, [
                                    _vm._v("Ativo")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "0" } }, [
                                    _vm._v("Inativo")
                                  ])
                                ]
                              )
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.formBot.active_exit && _vm.formBot.active_exit == 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _vm._m(34),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.formBot.porcent_profit,
                                    expression: "formBot.porcent_profit"
                                  }
                                ],
                                staticClass: "form-control form-control-sm",
                                attrs: { disabled: _vm.loading, type: "text" },
                                domProps: { value: _vm.formBot.porcent_profit },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.formBot,
                                      "porcent_profit",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.formBot.active_exit && _vm.formBot.active_exit == 1
                        ? _c("div", { staticClass: "col-12 col-md-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _vm._m(35),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.formBot.porcent_loss,
                                    expression: "formBot.porcent_loss"
                                  }
                                ],
                                staticClass: "form-control form-control-sm",
                                attrs: { disabled: _vm.loading, type: "text" },
                                domProps: { value: _vm.formBot.porcent_loss },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.formBot,
                                      "porcent_loss",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm._m(36),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _vm._m(37),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.formBot.max_value_day,
                                expression: "formBot.max_value_day"
                              }
                            ],
                            staticClass: "form-control form-control-sm",
                            attrs: { disabled: _vm.loading, type: "text" },
                            domProps: { value: _vm.formBot.max_value_day },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.formBot,
                                  "max_value_day",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ])
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-footer" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-sm btn-secondary",
                      attrs: {
                        type: "button",
                        disabled: _vm.loading,
                        "data-dismiss": "modal"
                      },
                      on: {
                        click: function($event) {
                          return _vm.resetModalBot()
                        }
                      }
                    },
                    [_vm._v("Fechar")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-sm btn-success",
                      attrs: { type: "button", disabled: _vm.loading },
                      on: {
                        click: function($event) {
                          return _vm.submitBot()
                        }
                      }
                    },
                    [_vm._v("Salvar")]
                  )
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c("notifications", {
        attrs: { group: "submit", position: "bottom center" }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("audio", { attrs: { id: "som_entrada", muted: "" } }, [
      _c("source", {
        attrs: { src: "/audios/correspondido.wav", type: "audio/wav" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("audio", { attrs: { id: "som_saida", muted: "" } }, [
      _c("source", { attrs: { src: "/audios/saida.wav", type: "audio/wav" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Status")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Nome")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Stake")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Odd Min")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Odd Max")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Gol(s) Casa")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Gol(s) Visitante")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Time min")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Time max")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Liquidity Market")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Ações")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("#ID")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Evento")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Placar")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Tempo")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("#ID Evento")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("#ID Mercado")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Evento")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Mercado Descrição")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Tipo Entrada")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Valor Entrada")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Valor Lucro")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Data")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h5", { staticClass: "modal-title", attrs: { id: "newbotLabel" } }, [
        _vm._v("Bot")
      ]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c(
        "span",
        { staticClass: "p-2 bg-secondary w-100 d-block my-3 text-white" },
        [_c("b", [_vm._v("Informações do Bot")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Status")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Nome")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Valor")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Mercado")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Tipo")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Quem")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c(
        "span",
        { staticClass: "p-2 bg-success w-100 d-block my-3 text-white" },
        [_c("b", [_vm._v("Critérios de Entrada")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Probailidade de acontecer % :")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Odd Min")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Odd Max")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Tempo Min")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Tempo Max")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Liquidex Mercado")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Max GAP ")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Gols Casa")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Gols Fora")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c(
        "span",
        { staticClass: "p-2 bg-secondary w-100 d-block my-3 text-white" },
        [_c("b", [_vm._v("Competições: ")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Todas as Competições")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Selecionar Competições")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Estatística")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Quem")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Mínimo")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Máximo")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Ativar/Desativar")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("GAP Saída")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Tempo de saída")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Saír se tomar GOL")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("b", [_vm._v("Saída % ( Lucro ) * Saí a qualquer momento. ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("b", [_vm._v("Saída % ( Perda ) * Saí a qualquer momento. ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c("span", { staticClass: "p-2 bg-info w-100 d-block my-3 text-white" }, [
        _c("b", [_vm._v("Máximo de Gastos Diários")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Valor Máximo Diário")])])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);