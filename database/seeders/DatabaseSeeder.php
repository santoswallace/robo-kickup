<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        User::updateOrcreate(
            ['id' => 1 ],
            [   
                'id' => 1,
                'name' => 'Wallace Santos',
                'email' => 'swallace@outlook.com.br',
                'password' => Hash::make('W@llace.22'), 
            ]
        );
        $this->call([
            CreateBotSeeder::class,
            CreateEstatisticasSeeder::class
        ]);
    }
}
