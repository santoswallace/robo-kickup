<?php

namespace Database\Seeders;

use App\Models\Bot;
use Illuminate\Database\Seeder;

class CreateBotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bot::updateOrcreate(
            ['id' => 1],
            [
                'status' => 1,
                'name' => 'BACK CASA VENCENDO 1-0',
                'type' => 'back',
                'value' => 3.50,
                'market' => 'MATCH_ODDS',
                'selection' => 0,
                'odd_min' => 1.11,
                'odd_max' => 1.70,
                'time_min' => 65,
                'time_max' => 70,
                'liquidity' => 10000.00,
                'max_gap' => 5,
                'score_home' => 1,
                'score_away' => 0,
                'active_exit' => 1,
                'gap_exit' => 8,
                'max_value_day' => 10,
                'time_exit' =>  72,
                'exit_take_gol' => 1,
                'porcent_profit' => 4,
                'porcent_loss' => 3,
            ]
        );

        Bot::updateOrcreate(
            ['id' => 2],
            [
                'status' => 1,
                'name' => 'UNDER 1,5 HT',
                'type' => 'back',
                'value' => 2,
                'market' => 'FIRST_HALF_GOALS_15',
                'selection' => 0,
                'odd_min' => 1.11,
                'odd_max' => 1.60,
                'time_min' => 24,
                'time_max' => 45,
                'liquidity' => 10000.00,
                'max_gap' => 5,
                'score_home' => 0,
                'score_away' => 0,
                'active_exit' => 1,
                'gap_exit' => 4,
                'max_value_day' => 10,
                'time_exit' =>  85,
                'exit_take_gol' => 1,
                'porcent_profit' => 4,
                'porcent_loss' => 3,
            ]
        );

        Bot::updateOrcreate(
            ['id' => 3],
            [
                'status' => 1,
                'name' => 'BACK VENCENDO 2-0',
                'type' => 'back',
                'value' => 3.50,
                'market' => 'MATCH_ODDS',
                'selection' => 0,
                'odd_min' => 1.11,
                'odd_max' => 1.50,
                'time_min' => 55,
                'time_max' => 65,
                'liquidity' => 10000.00,
                'max_gap' => 5,
                'score_home' => 2,
                'score_away' => 0,
                'active_exit' => 0,
                'gap_exit' => null,
                'max_value_day' => 10,
                'time_exit' =>  85,
                'exit_take_gol' => 1,
                'porcent_profit' => 4,
                'porcent_loss' => 3,
            ]
        );

        Bot::updateOrcreate(
            ['id' => 4],
            [
                'status' => 1,
                'name' => 'OVER 0.5 - TIRO CERTO',
                'type' => 'back',
                'value' => 1.50,
                'market' => 'OVER_UNDER_05',
                'selection' => 1,
                'odd_min' => 1.11,
                'odd_max' => 1.40,
                'time_min' => 0,
                'time_max' => 75,
                'liquidity' => 10000.00,
                'max_gap' => 5,
                'score_home' => 0,
                'score_away' => 0,
                'active_exit' => 1,
                'gap_exit' => 8,
                'max_value_day' => 10,
                'time_exit' =>  76,
                'exit_take_gol' => 1,
                'porcent_profit' => 4,
                'porcent_loss' => 3,
            ]
        );
    }
}
