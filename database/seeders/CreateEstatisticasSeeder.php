<?php

namespace Database\Seeders;

use App\Models\Statistic;
use Illuminate\Database\Seeder;

class CreateEstatisticasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Statistic::updateOrcreate(
            ['id' => 1],
            [
                'id' => 1,
                'bot_id' => 1,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Home',
                'min' => 3,
                'max' => 180
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 2],
            [
                'id' => 2,
                'bot_id' => 1,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Away',
                'min' => 0,
                'max' => 5
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 3],
            [
                'id' => 3,
                'bot_id' => 1,
                'statistic' => 'Corners',
                'side' => 'Home',
                'min' => 3,
                'max' => 20
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 4],
            [
                'id' => 4,
                'bot_id' => 1,
                'statistic' => 'Corners',
                'side' => 'Away',
                'min' => 0,
                'max' => 6
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 5],
            [
                'id' => 5,
                'bot_id' => 2,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Home+Away',
                'min' => 0,
                'max' => 8
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 6],
            [
                'id' => 6,
                'bot_id' => 2,
                'statistic' => 'Corners',
                'side' => 'Home+Away',
                'min' => 0,
                'max' => 5
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 7],
            [
                'id' => 7,
                'bot_id' => 2,
                'statistic' => 'ShotsOffTarget',
                'side' => 'Home',
                'min' => 0,
                'max' => 10
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 8],
            [
                'id' => 8,
                'bot_id' => 3,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Home',
                'min' => 3,
                'max' => 10
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 9],
            [
                'id' => 9,
                'bot_id' => 3,
                'statistic' => 'Corners',
                'side' => 'Home',
                'min' => 5,
                'max' => 20
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 10],
            [
                'id' => 10,
                'bot_id' => 3,
                'statistic' => 'DangerousAttacks',
                'side' => 'Home',
                'min' => 3,
                'max' => 80
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 11],
            [
                'id' => 11,
                'bot_id' => 3,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Away',
                'min' => 2,
                'max' => 5
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 12],
            [
                'id' => 12,
                'bot_id' => 3,
                'statistic' => 'Corners',
                'side' => 'Away',
                'min' => 1,
                'max' => 6
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 13],
            [
                'id' => 13,
                'bot_id' => 3,
                'statistic' => 'DangerousAttacks',
                'side' => 'Away',
                'min' => 3,
                'max' => 25
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 14],
            [
                'id' => 14,
                'bot_id' => 4,
                'statistic' => 'ShotsOnTarget',
                'side' => 'Home+Away',
                'min' => 3,
                'max' => 25
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 15],
            [
                'id' => 15,
                'bot_id' => 4,
                'statistic' => 'DangerousAttacks',
                'side' => 'Home+Away',
                'min' => 3,
                'max' => 100
            ]
        );

        Statistic::updateOrcreate(
            ['id' => 16],
            [
                'id' => 16,
                'bot_id' => 4,
                'statistic' => 'Corners',
                'side' => 'Home+Away',
                'min' => 3,
                'max' => 20
            ]
        );
    }

}
