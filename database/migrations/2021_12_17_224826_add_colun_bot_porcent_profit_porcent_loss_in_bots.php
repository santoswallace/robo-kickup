<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunBotPorcentProfitPorcentLossInBots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->unsignedBigInteger('porcent_profit')->after('exit_take_gol')->nullable();
            $table->unsignedBigInteger('porcent_loss')->after('porcent_profit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropColumn('porcent_profit');
            $table->dropColumn('porcent_loss');
        });
    }
}
