<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunExitTimeAndActiveTakeGolInBots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->unsignedBigInteger('time_exit')->after('gap_exit');
            $table->unsignedBigInteger('exit_take_gol')->after('time_exit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bots', function (Blueprint $table) {
           $table->dropColumn('time_exit');
           $table->dropColumn('exit_take_gol');
        });
    }
}
