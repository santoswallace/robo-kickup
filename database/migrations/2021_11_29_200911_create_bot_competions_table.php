<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotCompetionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot_competion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bot_id');
            $table->foreign('bot_id')->references('id')->on('bots');
            $table->unsignedBigInteger('competion_id');
            $table->foreign('competion_id')->references('id')->on('competions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot_competion');
    }
}
