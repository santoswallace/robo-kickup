<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bots', function (Blueprint $table) {
            $table->id();
            $table->boolean('status');
            $table->string('name');
            $table->string('type');
            $table->decimal('value', 10, 2);
            $table->decimal('max_value_day', 10, 2);
            $table->string('market');
            $table->string('selection');
            $table->decimal('odd_min', 10, 2);
            $table->decimal('odd_max', 10, 2);
            $table->bigInteger('time_min');
            $table->bigInteger('time_max');
            $table->decimal('liquidity', 14, 2);
            $table->bigInteger('max_gap');
            $table->bigInteger('score_home')->nullable();
            $table->bigInteger('score_away')->nullable();
            $table->boolean('active_exit')->default(1);
            $table->bigInteger('gap_exit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bots');
    }
}
