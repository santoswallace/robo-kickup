<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunPlacaInEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entradas', function (Blueprint $table) {
            $table->dropColumn('scores');
            $table->unsignedBigInteger('score_home')->after('marketName')->nullable();
            $table->unsignedBigInteger('score_away')->after('score_home')->nullable();
            $table->string('eventId')->after('score_away');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entradas', function (Blueprint $table) {
            $table->dropColumn('score_home');
            $table->dropColumn('score_away');
            $table->dropColumn('eventId');
            $table->string('scores')->after('marketName');
        });
    }
}
