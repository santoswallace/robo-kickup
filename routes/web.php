<?php

use App\Http\Controllers\BetController;
use App\Http\Controllers\BotController;
use App\Http\Controllers\GetController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProbabilidadeController;
use App\Services\Market\ServiceMarket;
use Illuminate\Support\Facades\Route;


Route::match(['get', 'post'], '', function () {
    return redirect('/login');
});

Route::match(['get', 'post'], 'register', function () {
    return redirect('/login');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {

    Route::prefix('get')->group(function () {
        Route::get('events', [IndexController::class, 'getEvents']);
        Route::get('account', [IndexController::class, 'getAccount']);
        Route::get('competions', [GetController::class, 'getCompetions']);

    });

    Route::prefix('bot')->group(function () {
        Route::get('get', [BotController::class, 'get']);
        Route::post('store', [BotController::class, 'store']);
        Route::post('update', [BotController::class, 'update']);
        Route::get('run/in', [BotController::class, 'In']);
        Route::get('run/exit', [BotController::class, 'Exit']);
        Route::get('delete/statistic/{id}', [BotController::class, 'deleteStatistic']);
    
    });

    Route::prefix('bet')->group(function () {
        Route::get('listClearedOrders', [BetController::class, 'listClearedOrders']);
    });

    Route::prefix('probabilidades')->group(function () {
        Route::get('/', [ProbabilidadeController::class, 'index']);
        Route::get('get/events/full', [ProbabilidadeController::class, 'getProbabilidade']);

    });


    Route::get('/home', [App\Http\Controllers\IndexController::class, 'index'])->name('home');

});
