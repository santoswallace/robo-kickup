<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrada extends Model
{
    use HasFactory;
    protected $fillable = [
        'bot_id',
        'marketId',
        'selectionId',
        'value',
        'odd',
        'exit',
        'type',
        'event',
        'marketName',
        'score_home',
        'score_away',
        'eventId'
    ];

    // public function bots(){
    //     return $this->belongsTo(Bot::class);
    // }
}
