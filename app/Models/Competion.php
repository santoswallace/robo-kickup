<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competion extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'competion_id',
        'name',
        'region',
    ];

    public function bots()
    {
        return $this->belongsToMany(Bot::class);
    }
}

