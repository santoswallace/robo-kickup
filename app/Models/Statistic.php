<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statistic extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'bot_id',
        'statistic',
        'side',
        'min',
        'max'
    ];


    public function bot(){
        return $this->belongsTo(Bot::class);
    }
}
