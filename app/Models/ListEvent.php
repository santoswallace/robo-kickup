<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListEvent extends Model
{
    use HasFactory, SoftDeletes;

    protected $cast = [
        'date_open'
    ];

    protected $fillable = [
        'event_id',
        'name',
        'country_id',
        'date_open',
        'timezone'
    ];

    public  function country ()
    {
        return $this->belongsTo(Country::class);
    }
    
}
