<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bot extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = [
        'competions_ids'
    ];
    
    protected $fillable = [
        'status',
        'name',
        'type',
        'value',
        'max_value_day',
        'market',
        'selection',
        'odd_min',
        'odd_max',
        'time_min',
        'time_max',
        'liquidity',
        'max_gap',
        'score_home',
        'score_away',
        'active_exit',
        'gap_exit',
        'time_exit',
        'exit_take_gol',
        'porcent_profit',
        'porcent_loss',
        'probability'
    ];

    public function Competions()
    {
        return $this->belongsToMany(Competion::class);
    }

    public function getCompetionsIdsAttribute()
    {
        $competionsIds = [];
        if (!empty($this->Competions)) {
            foreach ($this->Competions as $competion) {
                if (!in_array($competion['competion_id'], $competionsIds)) {
                    $competionsIds[] = $competion['competion_id'];
                }
            }
        }

        return $competionsIds;
    }

    public function entradas()
    {
        return $this->hasMany(Entrada::class);
    }

    public function statistics()
    {
        return $this->hasMany(Statistic::class);
    }
}
