<?php

namespace App\Services\Events;

use App\Models\TypeEvent;
use App\Services\Rest;
use Exception;

class ServiceTypeEvents
{

    public static function sync()
    {
        $data['filter']['textQuery'] = '';
        $response = Rest::exec($data, null, null, 'listEventTypes/');

        if (is_array($response) && count($response) > 0) {
            foreach ($response as $res) {
                TypeEvent::updateOrCreate(
                    ['name' => $res['eventType']['name']],
                    [
                        'name' => $res['eventType']['name'],
                        'market_count' => $res['marketCount']
                    ],
                );
            }
        }
    }

    public static function get($sync)
    {
        if($sync){
            Self::sync();
        }
        return TypeEvent::all();
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
