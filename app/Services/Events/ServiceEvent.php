<?php

namespace App\Services\Events;

use App\Models\Bot;
use App\Models\Competion;
use App\Models\Country;
use App\Models\ListEvent;
use App\Models\TypeEvent;
use App\Services\Events\Event as EventsEvent;
use App\Services\Market\ServiceMarket;
use App\Services\Rest;
use Carbon\Carbon;
use Exception;

class ServiceEvent
{


    public static function sync()
    {

        $bots = Bot::select('id', 'market')->with('competions')->get()->toArray();

        $competionsIds = [];
        foreach ($bots as $bot) {
            if (!empty($bot['competions'])) {
                foreach ($bot['competions'] as $competion) {
                    if (!in_array($competion['competion_id'], $competionsIds)) {
                        $competionsIds[] = $competion['competion_id'];
                    }
                }
            }
        }

        $marketsTypes = [];
        foreach ($bots as $bot) {
            if (!empty($bot)) {
                foreach ($bots as $bot) {
                    if (!in_array($bot['market'], $marketsTypes)) {
                        $marketsTypes[] = $bot['market'];
                    }
                }
            }
        }

        $data['filter']['textQuery'] = 'Soccer';
        $data['filter']['turnInPlayEnabled'] = true;
        $data['filter']['inPlayOnly'] = true;
        $data['filter']['marketTypeCodes'] = $marketsTypes;
        $data['filter']['competitionIds'] = $competionsIds;

        $response = Rest::exec($data, null, null, 'listEvents/');
        if (is_array($response)) {
            return $response;
        }
    }

    public static function get()
    {
        $events = Self::sync();
        if (!empty($events) && is_array($events)) {

            $eventsIds = array_map(function ($element) {
                return $element['event']['id'] ?? null;
            }, $events);

            $eventsIds = implode(',', $eventsIds);
            $statistics = Self::getStatistics($eventsIds);

            $listEvents = [];
            if (!empty($statistics) && is_array($statistics)) {

                foreach ($events as $event) {
                    foreach ($statistics as $stat) {
                        if (!empty($event['event']) && !empty($event['event']['id']) && $event['event']['id'] == $stat['eventId']) {
                            $event['event']['statistics'] = $stat;
                        }
                    }
                    $listEvents[] = $event;
                }
            }

            return $listEvents;
        }


        return $events;
    }

    public static function getStatistics($idEvent)
    {
        if (!empty($idEvent)) {
            $url = "https://ips.betfair.com/inplayservice/v1/eventTimelines?locale=pt&alt=json&eventIds={$idEvent}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $response = json_decode(curl_exec($ch), true);
            return $response;
        } else {
            return [];
        }
    }


    public static function getFullStatistics($idEvent)
    {
        if (!empty($idEvent)) {
            $url = "https://betfair.betstream.betgenius.com/betstream-view/footballscorecentre/betfairfootballscorecentre/json?eventId={$idEvent}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $response = json_decode(curl_exec($ch), true);
            return $response;
        } else {
            return [];
        }
    }


    /**
     * @method getFull
     * MEthodo que tras todos os dados de estatisticas inclusive probabilidade de vitoria dos mercados.
     */
    public function getFull()
    {
        set_time_limit(-1);
        $bots = Bot::whereStatus(1)->get();
        $competionsIds = [];
        $marketTypes = [];
        if ($bots) {
            foreach ($bots->toArray() as $bot) {
                if (!in_array($bot['market'], $marketTypes)) {
                    $marketTypes[] = $bot['market'];
                }
                if (!empty($bot['competions_ids'])) {
                    foreach ($bot['competions_ids'] as $competion) {
                        if (!in_array($competion, $competionsIds)) {
                            $competionsIds[] = $competion;
                        }
                    }
                }
            }
        }
        $catalogueMarket = ServiceMarket::catalogueMarket(null, $marketTypes, $competionsIds);
        $catalogues = [];
        $catalogues['marketIds'] = [];
        if (!empty($catalogueMarket)) {
            foreach ($catalogueMarket as $catalogue) {
                if (!empty($catalogue['marketId']) && !in_array($catalogue['marketId'], $catalogues['marketIds'])) {
                    $catalogues['marketIds'][] = $catalogue['marketId'];
                    $catalogues['eventsIds'][] = $catalogue['event']['id'];
                    $catalogues['detailsMarket'][$catalogue['marketId']] = $catalogue;
                    $catalogues['detailsMarket'][$catalogue['marketId']]['statistics'] = ServiceEvent::getFullStatistics($catalogue['event']['id']);
                }
            }
        }

        $markets = ServiceMarket::listMarketBook($catalogues['marketIds']);
        if (!empty($markets)) {
            foreach ($markets as $market) {

                //Valida que existe statisticas e que não estao incompletas
                if (!empty($catalogues['detailsMarket'][$market['marketId']]) && !empty($catalogues['detailsMarket'][$market['marketId']]['statistics']['Statistics']) && count($catalogues['detailsMarket'][$market['marketId']]['statistics']['Statistics']) > 2) {

                    $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_title'] = $catalogues['detailsMarket'][$market['marketId']]['marketName'];

                    if (!empty($catalogues['detailsMarket'][$market['marketId']]['statistics']['Scoreboard']['CurrentPeriod'])) {
                        $time_selection = $catalogues['detailsMarket'][$market['marketId']]['statistics']['Scoreboard']['CurrentPeriod'] == 'FIRST_HALF' ?  'FirstHalf' : 'SecondHalf';
                        $catalogues['detailsMarket'][$market['marketId']]['statistics']['time_current'] =  !empty($catalogues['detailsMarket'][$market['marketId']]['statistics']['Timeline'][$time_selection]['Normal']) ? $catalogues['detailsMarket'][$market['marketId']]['statistics']['Timeline'][$time_selection]['Normal'] : 'NI';
                        $catalogues['detailsMarket'][$market['marketId']]['statistics']['title_time'] =  $time_selection == 'FirstHalf' ? '1º Tempo' : '2º Tempo';
                    }

                    if (!empty($market['runners'][0]['lastPriceTraded']) && !empty($market['runners'][1]['lastPriceTraded'])) {

                        $catalogues['detailsMarket'][$market['marketId']]['statistics']['last_odd_back'] = $market['runners'][0]['lastPriceTraded'];
                        $catalogues['detailsMarket'][$market['marketId']]['statistics']['last_odd_lay'] = $market['runners'][1]['lastPriceTraded'];

                        if ($catalogues['detailsMarket'][$market['marketId']]['marketName'] === 'Match Odds') {
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_back_home'] = number_format(100 / $market['runners'][0]['lastPriceTraded'], 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_back_away'] = number_format(100 / $market['runners'][1]['lastPriceTraded'], 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_back_draw'] = number_format(100 / $market['runners'][2]['lastPriceTraded'], 2, '.', '');

                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_lay_home'] =  number_format(100 - (100 / $market['runners'][0]['lastPriceTraded']), 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_lay_away'] =  number_format(100 - (100 / $market['runners'][1]['lastPriceTraded']), 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_lay_draw'] =  number_format(100 - (100 / $market['runners'][2]['lastPriceTraded']), 2, '.', '');
                        } else {
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_back_under'] = number_format(100 / $market['runners'][0]['lastPriceTraded'], 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_back_over'] = number_format(100 / $market['runners'][1]['lastPriceTraded'], 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_lay_under'] =  number_format(100 - (100 / $market['runners'][0]['lastPriceTraded']), 2, '.', '');
                            $catalogues['detailsMarket'][$market['marketId']]['statistics']['probabilidade_lay_over'] =  number_format(100 - (100 / $market['runners'][1]['lastPriceTraded']), 2, '.', '');
                        }
                    }
                }else{
                    //Caso não tenha statisticas, remove do array
                    unset($catalogues['detailsMarket'][$market['marketId']]);
                }
            }
        }

        return $catalogues['detailsMarket'];
    }
}
