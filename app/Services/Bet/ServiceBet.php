<?php

namespace App\Services\Bet;

use App\Services\Events\ServiceEvent;
use App\Services\Rest;
use Carbon\Carbon;
use Exception;

class ServiceBet
{

    public static function sync()
    {
        // $data['betStatus'] = 'SETTLED';
        // $response = Rest::exec($data, null,null,'listClearedOrders/' );
        // if (is_array($response) && count($response) > 0) {
        //     return $response;
        // }
    }

    public static function get()
    {
        // return Self::sync();
    }

    public static function listClearedOrders($dateNow = true)
    {
    

        $data['betStatus'] = 'SETTLED';
        $data['includeItemDescription'] = true;
        $response = Rest::exec($data, null, null, 'listClearedOrders/');
        if (is_array($response) && count($response) > 0) {
            $list = [];
            if ($dateNow) {
                foreach ($response['clearedOrders'] as $res) {
                    if (date('Y-m-d', strtotime($res['placedDate'])) >= date('Y-m-d')) {
                        $res['placedDate'] = date('d/m/Y', strtotime($res['placedDate']));
                        $list[] = $res;
                    }
                }

                return $list;
            } else {
                return $response;
            }
        }
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
