<?php

namespace App\Services\Countries;

use App\Models\Country;
use App\Services\Rest;
use Exception;

class ServiceCountries
{

    public static function sync()
    {
        $data['filter']['textQuery'] = '';
        $response = Rest::exec($data, null, null, 'listCountries/');

        if (is_array($response) && count($response) > 0) {
            foreach ($response as $res) {
                Country::updateOrCreate(
                    ['name' => $res['countryCode']],
                    [
                        'name' => $res['countryCode'],
                        'market_count' => $res['marketCount']
                    ],
                );
            }
        }
    }

    public static function get($sync = false)
    {
        if($sync){
            Self::sync();
        }
        return Country::all();
    }



    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
