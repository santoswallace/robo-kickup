<?php

namespace App\Services\Competions;

use App\Models\Bot;
use App\Models\Competion;
use App\Services\Rest;
use Exception;

class ServiceCompetions
{

    public static function get()
    {
        $bots = Bot::select('id','market')->whereStatus(1)->get()->toArray();
        $marketsTypes = [];
        foreach($bots as $bot){
            if(!empty($bot)){
                foreach($bots as $bot){
                    if(!in_array($bot['market'], $marketsTypes)){
                        $marketsTypes[] = $bot['market'];
                    }
                }
            }
        }

        $data['filter']['textQuery'] = 'Soccer';

        $response = Rest::exec($data, null, null, 'listCompetitions/');
        if (is_array($response) && count($response) > 0) {
            foreach ($response as $res) {
                Competion::updateOrCreate(
                    ['competion_id' => $res['competition']['id']],
                    [
                        'competion_id' => $res['competition']['id'],
                        'name' => $res['competition']['name'],
                        'region' => $res['competitionRegion']
                    ],
                );
            }

            return Competion::all();
        }else{
            return [];
        }
    }


    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
