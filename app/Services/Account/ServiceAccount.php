<?php

namespace App\Services\Account;

use App\Services\Rest;
use Exception;

class ServiceAccount
{

    public static function sync()
    {
        $data['wallet'] = 'UK';
        $response = Rest::exec($data, null, 'https://api.betfair.com/exchange/account/rest/v1.0/', 'getAccountFunds/' );
        $response_ = Rest::exec(null, null, 'https://api.betfair.com/exchange/account/rest/v1.0/', 'getAccountDetails/' );
        if (is_array($response) && count($response) > 0) {
            return array_merge($response, $response_);
        }
    }

    public static function get()
    {   
        return Self::sync();
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
