<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Session;

class Rest
{   
    
    private static $application_key = 'ZQlRxJQf6OcWOl1J';
    private static $url = 'https://api.betfair.com/exchange/betting/rest/v1.0/';

    // Version ID: 98466 
    // Application Key Not Delay: ZQlRxJQf6OcWOl1J


    public static function exec($data = null, $method = 'POST', $url = null, $endpoint = null)
    {       
        $headers = array(
            'Accept: application/json',
            'X-Application: ' . self::$application_key,
        );

        //Muda o Content-Type:  quando for login passa  form-urlencoded e quando for requisição ja logado passa json
        if (!Session::has('sessionToken')) {
            array_push($headers, 'Content-Type: application/x-www-form-urlencoded');
        } else {
            array_push($headers, 'Content-Type: application/json');
            array_push($headers, 'X-Authentication: ' . Session::get('sessionToken'));
            $data = json_encode($data);
        }

        $ch = curl_init();
        $curl_url = $url ? $url . $endpoint : self::$url . $endpoint;
        
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);

        return $response;
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
