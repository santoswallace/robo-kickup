<?php

namespace App\Services\Market;

use App\Models\Bot;
use App\Models\Country;
use App\Models\ListEvent;
use App\Models\TypeMarket;
use App\Services\Rest;
use Carbon\Carbon;
use Exception;

class ServiceMarket
{

    public static function catalogueMarket(array $eventsIds = null, array $marketsTypes = null, array $competionsIds = null, $inPlay = true)
    {
        if ($eventsIds) {
            $data['filter']['eventIds'] = $eventsIds;
        }

        $data['filter']['marketTypeCodes'] = $marketsTypes;
        $data['filter']['competitionIds'] = $competionsIds;
        $data['filter']['textQuery'] = 'Soccer';
        $data['marketProjection'] = ['EVENT', 'COMPETITION'];
        $data['filter']['inPlayOnly'] = $inPlay;
        $data['maxResults'] = 100;
        $response = Rest::exec($data, null, null, 'listMarketCatalogue/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }

    public static function listMarketBook($marketIds)
    {
        $data['marketIds'] = $marketIds;
        $response = Rest::exec($data, null, null, 'listMarketBook/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }

    public static function marketProfitAndLoss($marketIds)
    {
        $data['marketIds'] = $marketIds;
        $data['includeSettledBets'] = true;
        $data['includeBspBets'] = true;
        $data['netOfCommission'] = true;
        $response = Rest::exec($data, null, null, 'listMarketProfitAndLoss/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }

    public static function listClearedOrders($marketIds)
    {
        $data['betStatus'] = 'SETTLED';
        $data['marketIds'] = $marketIds;
        $response = Rest::exec($data, null, null, 'listClearedOrders/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }

    public static function listRunnerBook($marketId, $selectionId)
    {
        $data['marketId'] = $marketId;
        $data['selectionId'] = $selectionId;
        $response = Rest::exec($data, null, null, 'listRunnerBook/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }


    public static function listCurrentOrders($marketIds)
    {
        $data['marketIds'] = $marketIds;
        $data['orderBy'] = 'BY_MARKET';
        $data['fromRecord'] = 0;
        $data['recordCount'] = 0;
        $response = Rest::exec($data, null, null, 'listCurrentOrders/');

        if (is_array($response) && count($response) > 0) {
            return $response;
        }
    }



    public static  function sync_type_markets()
    {
        $data['filter']['textQuery'] = 'Soccer';
        $response = Rest::exec($data, null, null, 'listMarketTypes/');

        if (is_array($response) && count($response) > 0) {

            foreach ($response as $res) {
                TypeMarket::updateOrcreate(['name' => $res['marketType']], ['name' => $res['marketType']]);
            }
            return $response;
        }
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
