<?php

namespace App\Http\Controllers;

use App\Models\Bot;
use App\Services\Account\ServiceAccount;
use App\Services\Events\ServiceEvent;
use App\Services\Market\ServiceMarket;
use App\Services\Rest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    public function index()
    {
        session_start();
        
        // $bot = Bot::find(1);
        // $bot->competions()->sync([82,90,48,44,60,68,57,47,50,46]);

        // $bot = Bot::find(2);
        // $bot->competions()->sync([82,90,48,44,60,68,57,47,50,46]);

        // $bot = Bot::find(3);
        // $bot->competions()->sync([82,90,48,44,60,68,57,47,50,46]);

        // $bot = Bot::find(4);
        // $bot->competions()->sync([82,90,48,44,60,68,57,47,50,46]);

        $this->login();

        if (!Session::has('sessionToken')) {
            $this->login();
        }

        if (!Session::has('bots')) {
            Session::put('bots', Bot::all());
        }

        return view('home');
    }

    public function getEvents()
    {
        return ServiceEvent::get();
    }

    public function getStatisticsEvents($idEvent)
    {
        return ServiceEvent::getStatistics($idEvent);
    }

    public function getAccount()
    {
        return ServiceAccount::get();
    }

    public function login()
    {
        $response = Rest::exec('username=swallace@outlook.com.br&password=W@llace.22', 'POST', 'https://identitysso.betfair.com/api/login');

        if (!empty($response['token']) && !empty($response['status']) && $response['status'] == 'SUCCESS') {
            Session::put('sessionToken', $response['token']);
        }
    }

    
}
