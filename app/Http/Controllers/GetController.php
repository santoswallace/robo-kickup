<?php

namespace App\Http\Controllers;

use App\Models\Competion;
use App\Services\Competions\ServiceCompetions;
use Illuminate\Http\Request;

class GetController extends Controller
{
    public function getCompetions(){
        return ServiceCompetions::get();
    }
}
