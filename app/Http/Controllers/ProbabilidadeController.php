<?php

namespace App\Http\Controllers;

use App\Models\Bot;
use App\Services\Events\ServiceEvent;
use App\Services\Market\ServiceMarket;
use Illuminate\Http\Request;

class ProbabilidadeController extends Controller
{
    public function index()
    {
        return view('probalidades');
    }


    public function getProbabilidade()
    {
        return ServiceEvent::getFull();
    }
}
