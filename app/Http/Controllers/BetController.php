<?php

namespace App\Http\Controllers;

use App\Services\Bet\ServiceBet;
use Illuminate\Http\Request;

class BetController extends Controller
{
    public function listClearedOrders()
    {
        return  ServiceBet::listClearedOrders();
      
    }

}
