<?php

namespace App\Http\Controllers;

use App\Models\Bot;
use App\Models\Competion;
use App\Models\Entrada;
use App\Models\Statistic;
use App\Services\Events\ServiceEvent;
use App\Services\Market\ServiceMarket;
use App\Services\Rest;
use App\Services\Telegram\ServiceTelegram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class BotController extends Controller
{
    public function store(Request $request)
    {
        if (Session::has('bots')) {
            Session::remove('bots');
        }

        $bot = Bot::create($request->all());

        if (!empty($request->allCompetions) && $request->allCompetions == 1) {
            $competionsIds = array_filter(array_map(function ($query) {
                return $query['id'];
            }, Competion::all()->toArray()));

            if ($competionsIds) {
                $bot->Competions()->sync($competionsIds);
            }
        } else {
            $bot->Competions()->sync($request->competions);
        }

        if (!empty($request->statistics)) {
            foreach ($request->statistics as $statistic) {
                Statistic::updateOrcreate(['id' => $statistic['id'] ?? null], [
                    'bot_id' => $bot['id'],
                    'statistic' => $statistic['statistic'],
                    'side' => $statistic['side'],
                    'min' => $statistic['min'],
                    'max' => $statistic['max'],
                ]);
            }
        }

        Session::put('bots', Bot::whereStatus(1)->with('Competions')->get());

        return $bot;
    }

    public function get()
    {
        return Bot::with('Competions', 'statistics')->get();
    }

    public function update(Request $request)
    {


        $bot = Bot::find($request['id']);
        $bot->update($request->except('created_at', 'updated_at', 'deleted_at', 'competions'));
        $bot->save();

        if (!empty($request->allCompetions) && $request->allCompetions == 1) {
            $competionsIds = array_filter(array_map(function ($query) {
                return $query['id'];
            }, Competion::all()->toArray()));

            if ($competionsIds) {
                $bot->Competions()->sync($competionsIds);
            }
        } else {

            $competionsIds = array_filter(array_map(function ($query) {
                return $query['id'];
            }, $request->competions));

            $bot->Competions()->sync($competionsIds);
        }

        if (!empty($request->statistics)) {
            foreach ($request->statistics as $statistic) {
                Statistic::updateOrcreate(['id' => $statistic['id'] ?? null], [
                    'bot_id' => $bot['id'],
                    'statistic' => $statistic['statistic'],
                    'side' => $statistic['side'],
                    'min' => $statistic['min'],
                    'max' => $statistic['max'],
                ]);
            }
        }

        if (Session::has('bots')) {
            Session::remove('bots');
        }



        $bot = Bot::with('Competions', 'statistics')->find($request['id']);
        Session::put('bots', Bot::whereStatus(1)->with('Competions')->get());
        return $bot;
    }

    //VERIFICA REGRAS DE MARKET
    public function In()
    {
        session_start();
        set_time_limit(-1);
        $bots = Bot::whereStatus(1)->with('competions', 'entradas', 'statistics')->get()->toArray();
        $contagem_entradas = 0;

        if (!empty($bots)) {
            foreach ($bots as $bot) {

                //VERIFICA VALOR DE ENTRADA PRA VE SE NAO GASTOU MT
                $valor_entrada_diario = Entrada::select('id', 'bot_id', 'value')->whereDate('created_at', '=', Carbon::now())->whereBotId($bot['id'])->get()->sum('value');

                if ($valor_entrada_diario >= $bot['max_value_day']) {
                    Bot::whereId($bot['id'])->update(['status' => 0]);
                    ServiceTelegram::sendMessage('Bot: ' . $bot['name'] . ' | Desativado gasto diário atingido');
                    Log::info('BOT DESATIVADO | BATEU VALOR DIA | BOT: ' . json_encode($bot));
                } else {

                    //RECUPERA AS ENTRADAS DO BOT E RETORNA SO OS MARKETIDS
                    $entradasIds = array_filter(array_map(function ($q) {
                        return $q['marketId'];
                    }, $bot['entradas']));

                    //BUSCA OS CATALOGOS DE MARKET
                    $catalogueMarket = ServiceMarket::catalogueMarket(null, [$bot['market']], $bot['competions_ids']);

                    //BUSCA OS MARKETIDS BASEADO NO TIPO DO MERCADO DO BOT E DAS COMPETICOES
                    $catalogues = [];
                    $catalogues['marketIds'] = [];
                    if (!empty($catalogueMarket)) {
                        foreach ($catalogueMarket as $catalogue) {
                            if (!empty($catalogue['marketId']) && !in_array($catalogue['marketId'], $catalogues['marketIds']) && !in_array($catalogue['marketId'], $entradasIds)) {
                                $catalogues['marketIds'][] = $catalogue['marketId'];
                                $catalogues['eventsIds'][] = $catalogue['event']['id'];
                                $catalogues['detailsMarket'][$catalogue['marketId']] = $catalogue;
                            }
                        }
                    }

                    //BUSCA ESTATISTICAS DO EVENTO
                    if (!empty($catalogues['eventsIds'])) {
                        foreach ($catalogues['detailsMarket'] as $key => $value) {
                            $catalogues['detailsMarket'][$key]['statistics'] = ServiceEvent::getFullStatistics($value['event']['id']);
                        }
                    }

                    //BUSCA OS DADOS DE CADA MERCADO (RUNNERS,ODDS,ETC.)
                    if (!empty($catalogues['marketIds'])) {

                        $listOrders = [];
                        $markets = ServiceMarket::listMarketBook($catalogues['marketIds']);

                        if (!empty($markets) && count($markets)) {
                            foreach ($markets as $market) {
                                if (!array_key_exists($market['marketId'], $listOrders) && ($market['status'] != 'SUSPENDED') || ($market['status'] != 'CLOSED')) {
                                    if (!empty($market['marketId'])) {
                                        $marketInfo = $catalogues['detailsMarket'][$market['marketId']];
                                        $marketInfo['runners']  = $market['runners'];
                                        $marketInfo['status']  = $market['status'];
                                        $marketInfo['totalMatched']  = $market['totalMatched'];
                                    }

                                    if (!empty($marketInfo) && !empty($marketInfo['statistics']['Statistics']) && !empty($marketInfo['statistics']['Timeline']) && $this->validStatistis($marketInfo, $bot)) {
                                        $selection = $bot['selection'];
                                        $listOrders[$market['marketId']]['marketId'] = $market['marketId'];
                                        $listOrders[$market['marketId']]['bot_id'] = $bot['id'];
                                        $listOrders[$market['marketId']]['selectionId'] = $market['runners'][$selection]['selectionId'];
                                        $listOrders[$market['marketId']]['value'] =  $bot['value'];
                                        $listOrders[$market['marketId']]['type'] =  $bot['type'];
                                        $listOrders[$market['marketId']]['odd_market'] = $market['runners'][$selection]['lastPriceTraded'];
                                        $listOrders[$market['marketId']]['event_name'] = $marketInfo['event']['name'];
                                        $listOrders[$market['marketId']]['marketName'] = $marketInfo['marketName'];
                                        $listOrders[$market['marketId']]['event_id'] = $marketInfo['event']['id'];
                                        $listOrders[$market['marketId']]['max_gap'] = $bot['max_gap'];
                                        $listOrders[$market['marketId']]['score_home'] = $bot['score_home'];
                                        $listOrders[$market['marketId']]['score_away'] = $bot['score_away'];
                                    }
                                }
                            }
                        }

                        foreach ($listOrders as $order) {
                            $response = $this->inMarket($order);
                            if ($response) {
                                $contagem_entradas++;
                            }
                        }
                    }

                    Log::info('RUN BOT: ' . $bot['name']);
                }
            }
        }

        return response()->json(!empty($contagem_entradas) && $contagem_entradas > 0 ? true : false, 200);
    }

    // FAZ A ENTRADA NO MERCADO
    public function inMarket(array $order)
    {

        $entrada_success = false;
        $placeOrder['marketId'] = $order['marketId'];
        $placeOrder['instructions'][] = [
            'selectionId' => $order['selectionId'],
            'orderType' => 'LIMIT',
            'handicap' => 0,
            'side' => strtoupper($order['type']),
            'limitOrder' => [
                'size' => $order['value'] < 10 ? 10 : $order['value'],
                'price' => $order['type'] == 'lay' ? 1.03 : 1000,
                'persistenceType' => 'LAPSE'
            ],
        ];

        $response = Rest::exec($placeOrder, 'POST', null, 'placeOrders/');

        if ($order['value'] < 10) {

            if (!empty($response['instructionReports'][0]['betId']) && $response['status'] == 'SUCCESS') {

                $responseCancel  =  $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId'], 10.00 - floatval($order['value']));
                $responseNewOrder = $this->replaceNewOrder($response['marketId'], $response['instructionReports'][0]['betId'], $order['odd_market'], $order['max_gap'], $order['type']);

                if ((!empty($responseNewOrder) && !empty($responseNewOrder['status']) && $responseNewOrder['status'] != 'SUCCESS') || (!empty($responseCancel) && !empty($responseCancel['status']) && $responseCancel['status'] != 'SUCCESS')) {
                    if (!empty($response)) {
                        $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
                    }
                    if (!empty($responseNewOrder) && !empty($response)) {
                        $responseCancel = $this->cancelOrder($response['marketId'], $responseNewOrder['instructionReports'][0]['placeInstructionReport']['betId']);
                    }
                    Log::info('ORDER CANCELADA  FALHA EM ENTRADA/CANCELAMENTO/SUBSTITUICAO : ' . json_encode($responseCancel) . ' | RESPONSE INICIAL: ' . json_encode($response) . ' | RESPONSE CANCEL: ' . json_encode($responseCancel) . ' | RESPONSE NEW ORDER: ' . json_encode($responseNewOrder));
                    return false;
                }

                if ($responseNewOrder['status'] == 'SUCCESS' &&  $responseNewOrder['instructionReports'][0]['status'] == 'SUCCESS' && $responseNewOrder['instructionReports'][0]['cancelInstructionReport']['status'] == 'SUCCESS' &&  $responseNewOrder['instructionReports'][0]['placeInstructionReport']['status'] == 'SUCCESS'  && $responseNewOrder['instructionReports'][0]['placeInstructionReport']['orderStatus']  == 'EXECUTION_COMPLETE' && $responseNewOrder['instructionReports'][0]['placeInstructionReport']['averagePriceMatched'] > 0.00) {

                    Entrada::create([
                        'bot_id' => $order['bot_id'],
                        'marketId' => $order['marketId'],
                        'selectionId' => $order['selectionId'],
                        'value' => $order['value'],
                        'odd' => $responseNewOrder['instructionReports'][0]['placeInstructionReport']['averagePriceMatched'],
                        'type' => $order['type'],
                        'event' => $order['event_name'],
                        'eventId' => $order['event_id'],
                        'marketName' => $order['marketName'],
                        'score_home' => $order['score_home'],
                        'score_away' => $order['score_away'],
                    ]);

                    $odd = $responseNewOrder['instructionReports'][0]['placeInstructionReport']['averagePriceMatched'] && $responseNewOrder['instructionReports'][0]['placeInstructionReport']['averagePriceMatched']   > 0 ? $responseNewOrder['instructionReports'][0]['placeInstructionReport']['averagePriceMatched'] : $order['odd_market'];

                    $placar = $order['score_home'] . ' X ' . $order['score_away'];

                    ServiceTelegram::sendMessage('Entrada realizada: ' . $order['event_name'] . ' |  ODD ENTRADA: ' . $odd . ' | VALOR:  ' . $order['value'] . ' | Mercado: ' . $order['marketName'] . ' | PLACAR: ' . $placar . ' | TIPO: ' . $order['type']);

                    $entrada_success = true;
                } else {
                    if (!empty($response)) {
                        $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
                    }
                    if (!empty($responseNewOrder) && !empty($response)) {
                        $responseCancel = $this->cancelOrder($response['marketId'], $responseNewOrder['instructionReports'][0]['placeInstructionReport']['betId']);
                    }
                    Log::info('ORDER CANCELADA POR NAO SER CORRESPONDIDA : ' . json_encode($responseCancel) . ' | RESPONSE INICIAL: ' . json_encode($response) . ' | RESPONSE CANCEL: ' . json_encode($responseCancel) . ' | RESPONSE NEW ORDER: ' . json_encode($responseNewOrder));
                }
            }
        } else {
            if ((!empty($response) && $response['instructionReports'][0]['orderStatus'] === 'EXECUTABLE' && $response['instructionReports'][0]['averagePriceMatched'] === 0.00) || (empty($response['status'])) || (!empty($response['status']) && $response['status'] != 'SUCCESS')) {
                if (!empty($response)) {
                    $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
                }
                Log::info('ORDER CANCELADA POR NAO SER CORRESPONDIDA : ' . json_encode($responseCancel));
            } else {

                $placar = $order['score_home'] . ' X ' . $order['score_away'];
                ServiceTelegram::sendMessage('Entrada realizada: ' . $order['event_name'] . ' |  ODD: ' . $order['odd_market'] . ' | VALOR:  ' . $order['value'] . ' | Mercado: ' . $order['marketName'] . ' | PLACAR: ' . $placar . ' | TIPO: ' . $order['type']);
            }
        }

        return !empty($entrada_success) ? $entrada_success : false;
    }

    //SAÍDA DO MERCADO VERIFICA ESTATISTICAS
    public function Exit()
    {
        session_start();
        set_time_limit(-1);

        $contagem_saida = 0;
        $bots = Bot::with('competions', 'entradas')
            ->whereHas('entradas', function ($q) {
                $q->whereNull('exit')
                    ->whereDate('created_at', '=', Carbon::now());
            })
            ->with(['entradas' => function ($q) {
                $q->whereNull('exit');
            }])
            ->whereActiveExit(1)
            ->get()
            ->toArray();


        if (!empty($bots)) {
            foreach ($bots as $bot) {

                foreach ($bot['entradas'] as $entrada) {
                    //BUSCA ESTATISTICAS
                    $marketInfo = ServiceEvent::getFullStatistics($entrada['eventId']);

                    if (empty($marketInfo) || empty($marketInfo['Scoreboard']) || empty($marketInfo['Timeline'])) {
                        continue;
                    } else {

                        //BUSCA DADOS ATUAIS DO MERCADO
                        $market = ServiceMarket::listMarketBook([$entrada['marketId']]);
                        if (($market[0]['status'] == 'SUSPENDED') || ($market[0]['status'] == 'CLOSED') || empty($market[0]['runners'][$bot['selection']]['lastPriceTraded'])) {
                            Log::error('Erro ao buscar dados do market para tentar efetuar a saída MARKET ID: ' . $entrada['marketId']);
                            continue;
                        }

                        //SAI SE ATINGIR DETERMINADAS PORCENTAGENS
                        $oddEntrada = isset($entrada['odd']) ? $entrada['odd'] : false;
                        $oddAtual = isset($market[0]['runners'][$bot['selection']]['lastPriceTraded']) ?  $market[0]['runners'][$bot['selection']]['lastPriceTraded'] : false;

                        //EFETUA MUDANÇA DA ODD PARA A ODD ATUAL DO MERCADO
                        $entrada['odd'] = $market[0]['runners'][$bot['selection']]['lastPriceTraded'];

                        if ($oddEntrada && $oddAtual) {

                            $porcentagem = $this->getPorcentagem(number_format($entrada['value'] * ($oddEntrada / $oddAtual - 1), 2, '.', ''), $entrada['value']);
                            if (isset($bot['porcent_profit']) && $porcentagem >= $bot['porcent_profit']) {
                                $this->closeIn($bot, $entrada, $contagem_saida, 'Porcentagem PROFIT: ' . $porcentagem . '% ');
                                Log::info('BOT EXIT RUN PORCENTAGEM PROFIT: ' . $bot['name'] . ' | ODD DE SAIDA: ' . $entrada['odd']);
                                continue;
                            }

                            if (isset($bot['porcent_loss']) && $porcentagem <= -$bot['porcent_loss']) {
                                $this->closeIn($bot, $entrada, $contagem_saida, 'Porcentagem LOSS: ' . $porcentagem . '% ', true);
                                Log::info('BOT EXIT RUN PORCENTAGEM LOSS: ' . $bot['name'] . ' | ODD DE SAIDA: ' . $entrada['odd']);
                                continue;
                            }
                        }

                        //VALIDA PRA CASO OS GOLS CADASTRADOS NO INICIO SEJAM DIFERENTES,  JÁ EFETUA A SAIDA 
                        
                        if ((isset($entrada['score_home']) || isset($entrada['score_away'])) && ($bot['exit_take_gol'])) {
                            if (isset($marketInfo['Statistics']['HomeGoals']) &&  $entrada['score_home'] != $marketInfo['Statistics']['HomeGoals']) {
                                $this->closeIn($bot, $entrada, $contagem_saida, 'Gol tomado  - Home');
                                Log::info('BOT EXIT RUN TAKE GOL: ' . $bot['name'] . ' | ODD DE SAIDA: ' . $entrada['odd']);
                                continue;
                            }

                            if (isset($marketInfo['Statistics']['AwayGoals']) && $entrada['score_away'] != $marketInfo['Statistics']['AwayGoals']) {
                                $this->closeIn($bot, $entrada, $contagem_saida, 'Gol tomado - Fora');
                                Log::info('BOT EXIT RUN TAKE GOL: ' . $bot['name'] . ' | ODD DE SAIDA: ' . $entrada['odd']);
                                continue;
                            }
                        }

                        //SE ATINGIR O TEMPO DE SAIDA,   EXECUTA a SAIDA
                        if (isset($bot['time_exit'])) {
                            if ($marketInfo['Scoreboard']['CurrentPeriod'] != 'HALF_TIME') {
                                $time_selection = $marketInfo['Scoreboard']['CurrentPeriod'] == 'FIRST_HALF' ?  'FirstHalf' : 'SecondHalf';
                                $time = $marketInfo['Timeline'][$time_selection]['Normal'];

                                if ($time >= $bot['time_exit']) {
                                    $this->closeIn($bot, $entrada, $contagem_saida, 'Tempo atingido');
                                    Log::info('BOT EXIT RUN: ' . $bot['name'] . ' | ODD DE SAIDA: ' . $entrada['odd']);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }

        return response()->json(!empty($contagem_saida) && $contagem_saida > 0 ? true : false, 200);
    }

    //METHODO RESPONSÁVEL POR SAIR DO MERCADO
    public function closeIn($bot, $entrada, $contagem_saida, $metodo_saida, $saida_porcent_loss = null)
    {

        $type = $entrada['type'];
        $placeOrder['marketId'] = $entrada['marketId'];
        $placeOrder['instructions'][] = [
            'selectionId' => $entrada['selectionId'],
            'orderType' => 'LIMIT',
            'handicap' => 0,
            'side' => $type == 'back' ? 'LAY' : 'BACK',
            'limitOrder' => [
                'size' => 10,
                'price' => $type == 'back' ? 1.03 : 1000,
                'persistenceType' => 'LAPSE'
            ],
        ];


        //Muda o type da saída pra sair. Se entrei a back, o type do incrementodds precisar ser lay 
        if ($saida_porcent_loss) {
            $type = $type == 'back' ? 'lay' : 'back';
        }

        $response = Rest::exec($placeOrder, 'POST', null, 'placeOrders/');
        if (!empty($response['instructionReports'][0]['betId']) && $response['status'] == 'SUCCESS') {

            $responseCancel  =  $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId'], 10.00 - floatval($entrada['value']));
            $responseNewOrder = $this->replaceNewOrder($response['marketId'], $response['instructionReports'][0]['betId'], $entrada['odd'], $bot['gap_exit'], $type);

            if (!empty($responseCancel) && !empty($responseCancel['status']) && $responseCancel['status'] != 'SUCCESS' || !empty($responseCancel) && !empty($responseCancel['status']) && $responseCancel['status'] != 'SUCCESS') {
                if (!empty($response['instructionReports'][0]['betId'])) {
                    $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
                }

                if ($responseNewOrder['instructionReports'][0]['placeInstructionReport']['betId']) {
                    $responseCancel = $this->cancelOrder($response['marketId'], $responseNewOrder['instructionReports'][0]['placeInstructionReport']['betId']);
                }
                Log::info('ORDER CANCELADA POR NAO SER CORRESPONDIDA : ' . json_encode($responseCancel));
            } else {

                if (!empty($responseNewOrder['status']) && $responseNewOrder['status'] == 'SUCCESS') {
                    $entrada = Entrada::find($entrada['id']);
                    $entrada->exit = Carbon::now();
                    $entrada->save();

                    $score_home = isset($entrada['score_home']) ? $entrada['score_home'] : 'NI';
                    $score_away = isset($entrada['score_away']) ? $entrada['score_away'] : 'NI';

                    ServiceTelegram::sendMessage('Saída Posicionada: ' . $entrada['event'] . ' | VALOR:  ' . $entrada['value'] . ' | Mercado: ' . $entrada['marketName'] . ' | PLACAR: ' . $score_home . ' X ' . $score_away . ' | TIPO: ' . $entrada['type'] . ' | ODD SAÍDA: ' . $responseNewOrder['instructionReports'][0]['placeInstructionReport']['instruction']['limitOrder']['price'] . ' | METODO DE SAIDA: ' . $metodo_saida);

                    $contagem_saida++;
                } else {
                    $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
                    $responseCancel = $this->cancelOrder($response['marketId'], $responseNewOrder['instructionReports'][0]['placeInstructionReport']['betId']);
                    Log::info('ORDER CANCELADA POR NAO SER CORRESPONDIDA : ' . json_encode($responseCancel));
                }
            }
        } else {
            if (!empty($response) && !empty($response['marketId']) && !empty($response['instructionReports'][0]['betId'])) {
                $responseCancel = $this->cancelOrder($response['marketId'], $response['instructionReports'][0]['betId']);
            }
            // Log::info('Saída Resultado: ' . json_encode($response));
        }


        $placeOrder = null;
    }

    // INCREMENTO DE ODDS
    public function incrementOdds($odd, $max_gap, $type)
    {
        $oddExplode = explode('.', $odd);

        if ($oddExplode[0] == '1') {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.01 : $odd + 0.01;
            }
        }

        if ($oddExplode[0] == '2') {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.02 : $odd + 0.02;
            }
        }

        if ($oddExplode[0] == '3') {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.05 : $odd + 0.05;
            }
        }

        if (in_array($oddExplode[0], ['4', '5'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.1 : $odd + 0.1;
            }
        }

        if (in_array($oddExplode[0], ['6', '7', '8', '9'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.2 : $odd +  0.2;
            }
        }


        if (in_array($oddExplode[0], ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 0.5 : $odd + 0.5;
            }
        }

        if (in_array($oddExplode[0], ['20', '21', '22', '23', '24', '25', '26', '27', '28', '29'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 1 : $odd + 1;
            }
        }

        if (in_array($oddExplode[0], ['30', '31', '33', '33', '34', '35', '36', '37', '38', '39', '41', '42' . '43', '44', '45', '46', '47', '48', '49'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 2 : $odd + 2;
            }
        }

        if (in_array($oddExplode[0], ['50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '61', '62' . '63', '64', '65', '66', '67', '68', '69', '71', '72' . '73', '74', '75', '76', '77', '78', '79', '81', '82' . '83', '84', '85', '86', '87', '88', '89', '91', '92' . '93', '94', '95', '96', '97', '98', '99'])) {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 5 : $odd + 5;
            }
        }

        if ($oddExplode[0] == '100') {
            for ($i = 0; $i < $max_gap; $i++) {
                $odd = $type == 'back' ? $odd - 10 : $odd + 10;
            }
        }

        return number_format($odd, 2, '.', '');
    }

    // BUSCANDO PORCENTAGEM  SOBRE VALOR BASE.
    public function getPorcentagem($resultado, $stakeInicial)
    {
        return round($resultado / $stakeInicial * 100) > 100 ? 100 : round($resultado / $stakeInicial * 100);
    }

    //CANCELAMENTO DE ORDER PARCIAL OU TOTAL
    public function cancelOrder($marketId, $betId, $value = null)
    {
        Log::info('RUN CANCELAMENTO DE ORDER');
        $cancelOrder['marketId'] = $marketId;
        $cancelOrder['instructions'][] = [
            'betId' => $betId
        ];

        if (!empty($value)) {
            $cancelOrder['instructions'][0]['sizeReduction'] = $value;
        }

        $responseCancel = Rest::exec($cancelOrder, 'POST', null, 'cancelOrders/');
        return $responseCancel;
    }

    //REPLACE NEW ORDER
    public function replaceNewOrder($marketId, $betId, $oddMarket, $gapmax, $ordertype)
    {
        $newOrder['marketId'] = $marketId;
        $newOrder['instructions'][] = [
            'betId' => $betId,
            'newPrice' => $this->incrementOdds($oddMarket, $gapmax, $ordertype)
        ];
        $responseNewOrder = Rest::exec($newOrder, 'POST', null, 'replaceOrders/');
        return $responseNewOrder;
    }

    //VALIDAÇÃO DE ESTATISTICAS
    public function validStatistis($marketInfo, $bot)
    {   
        //VERIFICA SE TEM VALIDACAO DE PROBABILIDADE

        if(!empty($bot['probability']) && !empty($marketInfo['runners'][$bot['selection']]['lastPriceTraded'])){
            $probability = 0;
            if($bot['type'] == 'back'){
                $probability = number_format(100 / $marketInfo['runners'][$bot['selection']]['lastPriceTraded'], 2, '.', '');
            }else{
                $probability = number_format(100 - (100 / $marketInfo['runners'][$bot['selection']]['lastPriceTraded']), 2, '.', '');
            }

            if($probability < $bot['probability'] ){
                Log::info('Bot: ' . $bot['name'] . ' | Probabilidade fora | Probabilidade de acontecer: '. $probability.'  - Probailidade do bot: '. $bot['probability']);
                return false;
            }
        }

        //VERIFICAR SE EXISTE MARKETINFO
        if (empty($marketInfo) || (empty($marketInfo['statistics'])) || (empty($marketInfo['statistics']['Scoreboard'])) || empty($marketInfo['statistics']['Scoreboard']['CurrentPeriod'])) {
            Log::info('Não existe marketinfo e/ou estatisticas EVENT ID: ' . $marketInfo['event']['id'] . ' | MARKET ID: ' . $marketInfo['marketId']);
            return false;
        }

        //VERIFICA TEMPO DA PARTIDA
        if (isset($bot['time_min']) && isset($bot['time_max'])) {
            $time_selection = $marketInfo['statistics']['Scoreboard']['CurrentPeriod'] == 'FIRST_HALF' ?  'FirstHalf' : 'SecondHalf';

            if (!isset($marketInfo['statistics']['Timeline'][$time_selection]['Normal'])) {
                Log::error('NAO TEM NORMA:' . json_encode($marketInfo['statistics']['Timeline'][$time_selection]));
                // Log::info('Bot: ' . $bot['name'] . ' | variavel "NORMAL" Não de finida ainda. | EVENTO ID: ' . $marketInfo['event']['id']);
                return false;
            }

            $time = $marketInfo['statistics']['Timeline'][$time_selection]['Normal'];
            if (!($time >= $bot['time_min'] && $time <= $bot['time_max'])) {
                Log::info('Bot: ' . $bot['name'] . ' | Não rodou  tempo incompativel com o configurado | CONFIGURADO: ' . $bot['time_min'] . ' - ' . $bot['time_max'] . ' | ATUAL: ' . $time);
                return false;
            }
        }

        //VERIFICA VALOR CORRESPONDIDO
        if (!($marketInfo['totalMatched'] >= $bot['liquidity'])) {
            Log::info('Bot: ' . $bot['name'] . ' | Não rodou  Mercado correspondido incompativel com o configurado. | CONFIGURADO: ' . $bot['liquidity'] . ' | ATUAL: ' . $marketInfo['totalMatched']);
            return false;
        }



        //VERIFICA ODDs SE ESTÃO DENTRO DOS PARAMETROS
        if (!($marketInfo['runners'][$bot['selection']]['lastPriceTraded'] >= $bot['odd_min'] &&  $marketInfo['runners'][$bot['selection']]['lastPriceTraded'] <= $bot['odd_max'])) {
            Log::info('Bot: ' . $bot['name'] . ' | Não rodou  ODD incompativel com o configurado. | CONFIGURADO: ' . $bot['odd_min'] . ' - ' . $bot['odd_max'] . ' | ATUAL: ' . $marketInfo['runners'][$bot['selection']]['lastPriceTraded']);
            return false;
        }

        //VALIDAÇÃO DE GOLS
        if (isset($bot['score_home']) || isset($bot['score_away'])) {

            if (isset($marketInfo['statistics']['Statistics']['HomeGoals']) && isset($bot['score_home']) && $bot['score_home'] != $marketInfo['statistics']['Statistics']['HomeGoals']) {
                Log::info('Bot: ' . $bot['name'] . ' | Não rodou  Gol Casa incompativel com o configurado. | CONFIGURADO: ' . $bot['score_home'] . ' | ATUAL: ' . $marketInfo['statistics']['Statistics']['HomeGoals']);
                return false;
            }

            if (isset($marketInfo['statistics']['Statistics']['AwayGoals']) && isset($bot['score_away']) && $bot['score_away'] != $marketInfo['statistics']['Statistics']['AwayGoals']) {
                Log::info('Bot: ' . $bot['name'] . ' | Não rodou  Gol Visitante incompativel com o configurado. | CONFIGURADO: ' . $bot['score_away'] . ' | ATUAL: ' . $marketInfo['statistics']['Statistics']['AwayGoals']);
                return false;
            }
        }

        //VALIDA  ESTATISTICAS DE JOGO
        if (!empty($bot['statistics'])) {
            foreach ($bot['statistics'] as $statistic) {

                if ($statistic['side'] == 'Home+Away' && isset($marketInfo['statistics']['Statistics']['Home' . $statistic['statistic']]) && isset($marketInfo['statistics']['Statistics']['Away' . $statistic['statistic']])) {
                    $total = $marketInfo['statistics']['Statistics']['Home' . $statistic['statistic']] + $marketInfo['statistics']['Statistics']['Away' . $statistic['statistic']];
                    if (!($total >= $statistic['min'] && $total <= $statistic['max'])) {
                        Log::info('Bot: ' . $bot['name'] . ' | Não rodou  Estatistica HOME+CASA  incompativel com o configurado | CONFIGURADO: MIN: ' . $statistic['min'] . ' - MAX: ' . $statistic['max'] . ' | ATUAL: ' . $total . ' | NOME :' . $statistic['statistic']);
                        return false;
                    }
                }

                if ($statistic['side'] != 'Home+Away') {
                    $total = $marketInfo['statistics']['Statistics'][$statistic['side'] . $statistic['statistic']];
                    if (!($total >= $statistic['min'] && $total <= $statistic['max'])) {
                        Log::info('Bot: ' . $bot['name'] . ' | Não rodou  Estatistica HOME+CASA  incompativel com o configurado | CONFIGURADO: MIN: ' . $statistic['min'] . ' - MAX: ' . $statistic['max'] . ' | ATUAL: ' . $total . ' | NOME :' . $statistic['statistic']);
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
